/*
   Tomasz Nowak
   nr albumu: 277117
*/

#ifndef AISDI_MAPS_TREEMAP_H
#define AISDI_MAPS_TREEMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>

#define AISDI_HOMEWORK // homework to process map breadth-first

#ifdef AISDI_HOMEWORK
#include <forward_list>
#endif

namespace aisdi
{

template <typename KeyType, typename ValueType>
class TreeMap
{
public:
    using key_type = KeyType;
    using mapped_type = ValueType;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = std::size_t;
    using reference = value_type&;
    using const_reference = const value_type&;

    class ConstIterator;
    class Iterator;
    friend class ConstIterator;
    friend class Iterator;
    using iterator = Iterator;
    using const_iterator = ConstIterator;

    TreeMap()
        : root(nullptr)
    {}

    TreeMap(std::initializer_list<value_type> list)
        : root(nullptr)
    {
        for(const auto &v : list)
            (*this)[v.first] = std::move(v.second);
    }

    TreeMap(const TreeMap& other)
        : root(nullptr)
    {
        const struct FunctorAddToMap
        {
            TreeMap &map;
            FunctorAddToMap(TreeMap &m) : map(m) {}
            void operator() (Node *n) { map[n->value.first] = n->value.second; }
        } fadd(*this);
        forEachVLR(other.root, fadd);
    }

    TreeMap(TreeMap&& other)
        : root(other.root)
    {
        other.root = nullptr;
    }

    ~TreeMap()
    {
        forEachLRV(root, [](Node *n){delete n;});
    }

    TreeMap& operator=(const TreeMap& other)
    {
        if(this == &other)
            return *this;
        forEachLRV(root, [](Node *n){delete n;});
        root = nullptr;
        const struct FunctorAddToMap
        {
            TreeMap &map;
            FunctorAddToMap(TreeMap &m) : map(m) {}
            void operator() (Node *n) { map[n->value.first] = n->value.second; }
        } fadd(*this);
        forEachVLR(other.root, fadd);
        return *this;
    }

    TreeMap& operator=(TreeMap&& other)
    {
        if(this == &other)
            return *this;
        forEachLRV(root, [](Node *n){delete n;});
        root = other.root;
        other.root = nullptr;
        return *this;
    }

    bool isEmpty() const
    {
        return root==nullptr;
    }

    mapped_type& operator[](const key_type& key)
    {
        if(root==nullptr)
        {
            root = new Node(value_type(key, mapped_type()), nullptr);
            return root->value.second;
        }
        Node *n = root;
        while(n->value.first != key)
        {
            if(n->value.first < key)
            {
                if(n->left == nullptr)
                {
                    n->left = new Node(value_type(key, mapped_type()), n);
                    return n->left->value.second;
                }
                n = n->left;
            }
            else
            {
                if(n->right == nullptr)
                {
                    n->right = new Node(value_type(key, mapped_type()), n);
                    return n->right->value.second;
                }
                n = n->right;
            }
        }
        return n->value.second;
    }

    const mapped_type& valueOf(const key_type& key) const
    {
        return privateValueOf(key); // see privateValueOf for explaination
    }

    mapped_type& valueOf(const key_type& key)
    {
        return const_cast<mapped_type&>(privateValueOf(key)); // to reduce code duplication
    }

    const_iterator find(const key_type& key) const
    {
        return privateFind(key); // see privateFind for explaination
    }

    iterator find(const key_type& key)
    {
        return iterator(privateFind(key)); // see privateFind for explaination
    }

    void remove(const key_type& key)
    {
        remove(privateFind(key));
    }

    void remove(const const_iterator& it)
    {
        if(it.node == nullptr)
            throw std::out_of_range("Attempt to remove tree map end.");
        if(it.node->right == nullptr && it.node->left == nullptr)
            removeLeaf(it.node);
        else if(it.node->right == nullptr)
            removeNodeWithoutRightChild(it.node);
        else if(it.node->left == nullptr)
            removeNodeWithoutLeftChild(it.node);
        else
            removeNodeWithChildren(it.node);
    }

    size_type getSize() const
    {
        struct SizeFunctor
        {
            size_type size = 0;
            void operator() (Node *n) { (void)n; ++size; }
        } sf;
        forEachLRV(root, sf);
        return sf.size;
    }

    bool operator==(const TreeMap& other) const
    {
        size_type size = 0; // not calling getSize(), because it would duplicate iteration in *this
        for(auto it = begin(); it != end(); ++it)
        {
            auto found = other.find(it->first);
            if(found == other.end())
                return 0;
            if(found->second != it->second)
                return 0;
            ++size; // calculate *this size while iterating
        }
        if(size != other.getSize())
            return 0;
        return 1;
    }

    bool operator!=(const TreeMap& other) const
    {
        return !(*this == other);
    }

    iterator begin()
    {
        return iterator(cbegin());
    }

    iterator end()
    {
        return iterator(cend());
    }

    const_iterator cbegin() const
    {
        if(root == nullptr)
            return cend();
        Node *n = root;
        while(n->left)
            n = n->left; // traversal in-order - leftmost node is the first one
        return const_iterator(n, this);
    }

    const_iterator cend() const
    {
        return const_iterator(nullptr, this);
    }

    const_iterator begin() const
    {
        return cbegin();
    }

    const_iterator end() const
    {
        return cend();
    }

#ifdef AISDI_HOMEWORK
private:
    struct TmpData
    {
        const value_type &value;
        unsigned depth;

        TmpData(const value_type &v, unsigned d)
            : value(v), depth(d)
        {}

        bool thisIsLessThan(const TmpData &d)
        {
            // compare according to: 1. depth 2. key
            if(depth<d.depth)
                return 1;
            if(depth>d.depth)
                return 0;
            if(value.first<d.value.first) // value.first is key in map
                return 1;
            return 0;
        }

        void addToListAscending(std::forward_list<TmpData> &list)
        {
            auto it1 = list.begin();
            if(it1 == list.end())
            {
                list.push_front(*this);
                return;
            }
            if(thisIsLessThan(*it1))
            {
                list.push_front(*this);
                return;
            }
            auto it2 = it1;
            for(++it1; it1 != list.end(); ++it1)
            {
                if(thisIsLessThan(*it1))
                {
                    list.insert_after(it2, *this);
                    return;
                }
                it2 = it1;
            }
            list.insert_after(it2, *this);
        }
    }; // struct TmpData
    struct Node; // declare - to use pointer in function below
    void addToBreadthList(Node *n, unsigned depth, std::forward_list<TmpData> &list)
    {
        if(n==nullptr)
            return;
        TmpData td(n->value, depth);
        td.addToListAscending(list);
        addToBreadthList(n->left, depth+1, list);
        addToBreadthList(n->right, depth+1, list);
    }

public:
    template<typename Functor> // Functor must have operator() (const value_type&)
    void forEachBreadthFirst(Functor &f)
    {
        std::forward_list<TmpData> list;
        addToBreadthList(root, 0, list);
        for(const auto &it : list)
            f(it.value);
    }
#endif

private:
    struct Node
    {
        value_type value;
        Node *parent, *left, *right;

        Node (const value_type &v, Node *p)
            : value(v), parent(p), left(nullptr), right(nullptr)
        {}

        Node (value_type &&v, Node *p)
            : value(std::move(v)), parent(p), left(nullptr), right(nullptr)
        {}
    } *root;

    const mapped_type& privateValueOf(const key_type& key) const
    {
        // This function is created, becuse attempt to reuse one valueOf in another causes endless
        // recursion (function overloading does not work in this case - another name is needed).
        Node *n = root;
        while(n)
        {
            if(n->value.first == key)
                return n->value.second;
            if(n->value.first < key)
                n = n->left;
            else
                n = n->right;
        }
        throw std::out_of_range("Request for valueOf non-existing key.");
    }

    const_iterator privateFind(const key_type& key) const
    {
        // The same situation as in privateValueOf - reusability without endless recursion.
        Node *n = root;
        while(n)
        {
            if(n->value.first == key)
                return const_iterator(n, this);
            if(n->value.first < key)
                n = n->left;
            else
                n = n->right;
        }
        return cend();
    }

    void removeLeaf(const Node *n)
    {
        if(n == root)
            root = nullptr;
        else if(n == n->parent->left)
            n->parent->left = nullptr;
        else
            n->parent->right = nullptr;
        delete n;
    }

    void removeNodeWithoutLeftChild(const Node *n)
    {
        n->right->parent = n->parent;
        if(n == root)
            root = n->right;
        else if(n == n->parent->left)
            n->parent->left = n->right;
        else
            n->parent->right = n->right;
        delete n;
    }

    void removeNodeWithoutRightChild(const Node *n)
    {
        n->left->parent = n->parent;
        if(n == root)
            root = n->left;
        else if(n == n->parent->left)
            n->parent->left = n->left;
        else
            n->parent->right = n->left;
        delete n;
    }

    void removeNodeWithChildren(const Node *n)
    {
        Node *to_swap = n->left;
        while(to_swap->right)
            to_swap = to_swap->right;
        // Do not swap values, change pointers instead, because swapping to_swap->value with
        // n->value (and deleting to_swap) would invalidate iterator to 'innocent' to_swap element.
        if(to_swap != n->left) // if there was at least one iteration in 'while(to_swap->right)'
        {
            to_swap->parent->right = to_swap->left; // can be nullptr - nothing wrong with it
            if(to_swap->left)
                to_swap->left->parent = to_swap->parent;
            to_swap->left = n->left;
            n->left->parent = to_swap;
        }
        to_swap->right = n->right;
        n->right->parent = to_swap;
        to_swap->parent = n->parent;
        if(n == root)
            root = to_swap; // deleted node has no parent - change root pointer instead
        else if(n == n->parent->left)
            n->parent->left = to_swap; // make deleted node parent's pointer left valid
        else
            n->parent->right = to_swap; // make deleted node parent's pointer right valid
        delete n; // finally, after all that pointer changing...
    }

    // Templates to recursively call given functor on a subtree, starting at given node:
    // LRV - first left subtree, then right subtree, then call functor.
    // VLR - first call functor, then left subtree, then right subtree.
    template <typename Functor>
    static void forEachLRV(Node *n, Functor &f) // static because it does not use TreeMap methods
    {
        if(n==nullptr)
            return;
        forEachLRV(n->left, f);
        forEachLRV(n->right, f);
        f(n);
    }

    template <typename Functor>
    static void forEachLRV(Node *n, const Functor &f)
    {
        forEachLRV(n, const_cast<Functor&>(f));
    }

    template <typename Functor>
    static void forEachVLR(Node *n, Functor &f)
    {
        if(n==nullptr)
            return;
        f(n);
        forEachVLR(n->left, f);
        forEachVLR(n->right, f);
    }

    template <typename Functor>
    static void forEachVLR(Node *n, const Functor &f)
    {
        forEachVLR(n, const_cast<Functor&>(f));
    }
}; // class TreeMap

template <typename KeyType, typename ValueType>
class TreeMap<KeyType, ValueType>::ConstIterator
{
    friend class TreeMap;
public:
    using reference = typename TreeMap::const_reference;
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = typename TreeMap::value_type;
    using pointer = const typename TreeMap::value_type*;

    explicit ConstIterator()
    {}

    ConstIterator(const ConstIterator& other)
        : node(other.node), map(other.map)
    {}

    ConstIterator& operator++()
    {
        if(node == nullptr)
            throw std::out_of_range("operator++() on tree map end iterator.");
        // Convention - traversal in-order (first element - leftmost, last element - rightmost)
        if(node->right)
        {
            node = node->right;
            while(node->left)
                node = node->left; // go to the leftmost node in the subtree
            return *this;
        }
        for(;;)
        {
            if(node->parent == nullptr) // starting node was at the end of the rightmost branch - end
            {
                node = nullptr;
                return *this;
            }
            if(node == node->parent->right) // node is the 'right son'
                node = node->parent; // go up one node
            else
                break;
        }
        // After the loop (in particular, there might be 0 iterations), node is the 'left son'.
        node = node->parent;
        return *this;
    }

    ConstIterator operator++(int)
    {
        if(node == nullptr)
            throw std::out_of_range("operator++(int) on tree map end iterator.");
        auto retval = *this;
        // Convention - traversal in-order (first element - leftmost, last element - rightmost)
        if(node->right)
        {
            node = node->right;
            while(node->left)
                node = node->left; // go to the leftmost node in the subtree
            return retval;
        }
        for(;;)
        {
            if(node->parent == nullptr) // starting node was at the end of the rightmost branch - end
            {
                node = nullptr;
                return retval;
            }
            if(node == node->parent->right) // node is the 'right son'
                node = node->parent; // go up one node
            else
                break;
        }
        // After the loop (in particular, there might be 0 iterations), node is the 'left son'.
        node = node->parent;
        return retval;
    }

    ConstIterator& operator--()
    {
        if(node == nullptr)
        {
            node = map->root;
            if(node == nullptr)
                throw std::out_of_range("operator--() on tree map begin iterator.");
            while(node->right)
                node = node->right;
            return *this;
        }
        if(node->left)
        {
            node = node->left;
            while(node->right)
                node = node->right;
            return *this;
        }
        for(;;)
        {
            if(node->parent == nullptr) // staring node was at the end of the leftmost branch - begin
                throw std::out_of_range("operator--() on tree map begin iterator.");
            if(node == node->parent->left) // node is the 'left son'
                node = node->parent;
            else
                break;
        }
        // After the loop (in particular, there might be 0 iterations), node is the 'right son'.
        node = node->parent;
        return *this;
    }

    ConstIterator operator--(int)
    {
        auto retval = *this;
        if(node == nullptr)
        {
            node = map->root;
            if(node == nullptr)
                throw std::out_of_range("operator--(int) on tree map begin iterator.");
            while(node->right)
                node = node->right;
            return retval;
        }
        if(node->left)
        {
            node = node->left;
            while(node->right)
                node = node->right;
            return retval;
        }
        for(;;)
        {
            if(node->parent == nullptr) // staring node was at the end of the leftmost branch - begin
                throw std::out_of_range("operator--(int) on tree map begin iterator.");
            if(node == node->parent->left) // node is the 'left son'
                node = node->parent;
            else
                break;
        }
        // After the loop (in particular, there might be 0 iterations), node is the 'right son'.
        node = node->parent;
        return retval;
    }

    reference operator*() const
    {
        if(node == nullptr)
            throw std::out_of_range("operator*() on tree map end iterator.");
        return node->value;
    }

    pointer operator->() const
    {
        return &this->operator*();
    }

    bool operator==(const ConstIterator& other) const
    {
        return node==other.node && map==other.map; // comparison of map pointers is needed in case of end iterators
    }

    bool operator!=(const ConstIterator& other) const
    {
        return !(*this == other);
    }
private:
    TreeMap::Node *node;
    const TreeMap *map;

    ConstIterator(TreeMap::Node *n, const TreeMap *m)
        : node(n), map(m)
    {}
}; // class TreeMap<KeyType, ValueType>::ConstIterator

template <typename KeyType, typename ValueType>
class TreeMap<KeyType, ValueType>::Iterator : public TreeMap<KeyType, ValueType>::ConstIterator
{
public:
    using reference = typename TreeMap::reference;
    using pointer = typename TreeMap::value_type*;

    explicit Iterator()
    {}

    Iterator(const ConstIterator& other)
        : ConstIterator(other)
    {}

    Iterator& operator++()
    {
        ConstIterator::operator++();
        return *this;
    }

    Iterator operator++(int)
    {
        auto result = *this;
        ConstIterator::operator++();
        return result;
    }

    Iterator& operator--()
    {
        ConstIterator::operator--();
        return *this;
    }

    Iterator operator--(int)
    {
        auto result = *this;
        ConstIterator::operator--();
        return result;
    }

    pointer operator->() const
    {
        return &this->operator*();
    }

    reference operator*() const
    {
        // ugly cast, yet reduces code duplication.
        return const_cast<reference>(ConstIterator::operator*());
    }
}; // class TreeMap<KeyType, ValueType>::Iterator : public TreeMap<KeyType, ValueType>::ConstIterator

} // namespace aisdi

#endif /* AISDI_MAPS_MAP_H */
