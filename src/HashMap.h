/*
   Tomasz Nowak
   nr albumu: 277117
*/

#ifndef AISDI_MAPS_HASHMAP_H
#define AISDI_MAPS_HASHMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <list>

namespace aisdi
{

template <typename KeyType, typename ValueType>
class HashMap
{
public:
    using key_type = KeyType;
    using mapped_type = ValueType;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = std::size_t;
    using reference = value_type&;
    using const_reference = const value_type&;

    class ConstIterator;
    class Iterator;
    friend class ConstIterator;
    friend class Iterator;
    using iterator = Iterator;
    using const_iterator = ConstIterator;

    HashMap()
    {
        // allocated dynamically, because heap size is much greater than stack size
        hash_table = new Bucket[TAB_SIZE];
    }

    HashMap(std::initializer_list<value_type> list)
    {
        hash_table = new Bucket[TAB_SIZE];
        for(const auto &v : list)
            (*this)[v.first] = std::move(v.second);
    }

    HashMap(const HashMap& other)
    {
        hash_table = new Bucket[TAB_SIZE];
        for(size_type i=0; i<TAB_SIZE; ++i)
        {
            auto &list = hash_table[i];
            for(const auto &el : other.hash_table[i])
                list.insert(list.end(), el);
        }
    }

    HashMap(HashMap&& other)
        : hash_table(other.hash_table)
    {
        other.hash_table = new Bucket[TAB_SIZE];
    }

    ~HashMap()
    {
        delete [] hash_table;
    }

    HashMap& operator=(const HashMap& other)
    {
		if(this == &other)
			return *this;
        for(size_type i=0; i<TAB_SIZE; ++i)
        {
            auto &list = hash_table[i];
            list.clear();
            for(const auto &el : other.hash_table[i])
                list.insert(list.end(), el);
        }
        return *this;
    }

    HashMap& operator=(HashMap&& other)
    {
		if(this == &other)
			return *this;
        for(size_type i=0; i<TAB_SIZE; ++i)
            hash_table[i].clear();
        std::swap(hash_table, other.hash_table);
        return *this;
    }

    bool isEmpty() const
    {
        for(size_type i=0; i<TAB_SIZE; ++i)
        {
            if(!hash_table[i].empty())
                return 0;
        }
        return 1;
    }

    mapped_type& operator[](const key_type& key)
    {
        // Keys in list are in ascending order, so it is easier to check for non-existing key.
        // If there is no element with given key, it is created.
        auto &list = hash_table[getHash(key)];
        auto it = list.begin();
        for(; it!=list.end(); ++it)
        {
            if(key == it->first)
                return it->second;
            if(key < it->first)
                return list.emplace(it, key, mapped_type())->second;
        }
        return list.emplace(it, key, mapped_type())->second;
    }

    const mapped_type& valueOf(const key_type& key) const
    {
        return privateValueOf(key); // see privateValueOf for explaination
    }

    mapped_type& valueOf(const key_type& key)
    {
        return const_cast<mapped_type&>(privateValueOf(key)); // to reduce code duplication
    }

    const_iterator find(const key_type& key) const
    {
        return privateFind(key); // see privateFind for explaination
    }

    iterator find(const key_type& key)
    {
        return iterator(privateFind(key)); // see privateFind for explaination
    }

    void remove(const key_type& key)
    {
        remove(privateFind(key));
    }

    void remove(const const_iterator& it)
    {
        // Warning - remove invalidates pointer. I will not change this function prototype,
        // but in my opinion it should return the next valid pointer, like in STL.
        // To avoid pointer invalidation, remove can be called: map.remove(it++);
        if(it.it == it.list->end())
            throw std::out_of_range("Attepmt to remove element out of range.");
        it.list->erase(it.it);
    }

    size_type getSize() const
    {
        size_type retval = 0;
        for(size_type i=0; i<TAB_SIZE; ++i)
            retval += hash_table[i].size();
        return retval;
    }

    bool operator==(const HashMap& other) const
    {
        for(size_type i=0; i<TAB_SIZE; ++i)
        {
            const auto &list1= hash_table[i];
            const auto &list2 = other.hash_table[i];
            auto it2 = list2.begin();
            for(auto it1=list1.begin(); it1!=list1.end(); ++it1, ++it2)
            {
                if(it2 == list2.end()) // end of list2, no end of list1
                    return 0;
                if(it1->second!=it2->second || it1->first!=it2->first) // lists' elements not equal
                    return 0;
            }
            if(it2!=list2.end()) // end of list1, no end of list2
                return 0;
        }
        return 1; // everything in *this equals other
    }

    bool operator!=(const HashMap& other) const
    {
        return !(*this == other);
    }

    iterator begin()
    {
        return iterator(cbegin());
    }

    iterator end()
    {
        return iterator(cend());
    }

    const_iterator cbegin() const
    {
        // Find first non-empty list (if there is any).
        Bucket *list = hash_table;
        const Bucket *last_list = hash_table + TAB_SIZE - 1;
        auto it = list->begin();
        while(it == list->end())
        {
            if(list == last_list) // for all lists begin==end - empty HashMap
                break;
            it = (++list)->begin();
        }
        return const_iterator(it, list, this);
    }

    const_iterator cend() const
    {
        // Note that list in end iterator is always pointer to the last list, even if there are empty
        // lists with lower indices in hash_table. This convention makes it easier to implement operator--.
        Bucket *last_list = hash_table + TAB_SIZE - 1;
        return const_iterator(last_list->end(), last_list, this);
    }

    const_iterator begin() const
    {
        return cbegin();
    }

    const_iterator end() const
    {
        return cend();
    }
    
private:
    static const size_type TAB_SIZE = 9901;
    // Buckets are lists in case there are many elements with the same hashes.
    // I would use std::forward_list if there was no requirement for operator-- in iterator.
    using Bucket = std::list<value_type>;
    Bucket *hash_table; // allocated dynamically, because heap size is much greater than stack size
    std::hash<key_type> hash_functor;

    size_type getHash(const key_type &k) const
    {
        return hash_functor(k)%TAB_SIZE;
    }

    const_iterator privateFind(const key_type& key) const
    {
        // This is exaclty how const_iterator find(const key_type& key) const
        // should look like. I decided to make another function with another name
        // to enable code reusability in function iterator find(const key_type& key).
        // 'const' find can't be used in 'non const' find, because
        // the following call causes stack overflow (endless recursion):
        // const_iterator it = find(key); // calls the 'non const find' instead of 'const' one
        // return iterator(it);
        // The reason for this is probably the fact that function overloading is based
        // on arguments, not returned values, so function 'const find' from above is not called,
        // because *this in 'const find' is const and in 'non const find' - non const.
        Bucket *list = hash_table+getHash(key); // pointer will be needed to create iterator
        for(auto it = list->begin(); it!=list->end(); ++it)
        {
            if(key == it->first)
                return const_iterator(it, list, this);
            if(key < it->first)
                return cend();
        }
        return cend();
    }

    const mapped_type& privateValueOf(const key_type& key) const
    {
        // The same situation as in privateFind - this function is created, becuse
        // attempt to reuse one valueOf in another causes endless recursion.

        // Keys in list are in ascending order, so it is easier to check for non-existing key.
        // If there is no element with given key, exception is thrown (unlike in operator[]).
        auto &list = hash_table[getHash(key)];
        for(auto it = list.begin(); it!=list.end(); ++it)
        {
            if(key == it->first)
                return it->second;
            if(key < it->first)
                throw std::out_of_range("Request for valueOf non-existing key.");
        }
        throw std::out_of_range("Request for valueOf non-existing key.");
    }
}; // class HashMap

template <typename KeyType, typename ValueType>
class HashMap<KeyType, ValueType>::ConstIterator
{
    friend class HashMap;
public:
    using reference = typename HashMap::const_reference;
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = typename HashMap::value_type;
    using pointer = const typename HashMap::value_type*;

    explicit ConstIterator()
    {}

    ConstIterator(const ConstIterator& other)
        : it(other.it), list(other.list), map(other.map)
    {}

    ConstIterator& operator++()
    {
        if(it==list->end())
            throw std::out_of_range("operator++() on hash map end iterator.");
        const HashMap::Bucket *last_list = map->hash_table + map->TAB_SIZE - 1;
        // First iteration of loop below - it might be the end of a non-empty list,
        // other iterations - there might be many empty lists in hash_table.
        while(++it==list->end())
        {
                if(list == last_list) // end of hash_table in map
                    return *this; // end iterator: it=last_list->end()
                it = (++list)->begin();
        }
        return *this;
    }

    ConstIterator operator++(int)
    {
        if(it==list->end())
            throw std::out_of_range("operator++(int) on hash map end iterator.");
        auto retval = *this;
        const HashMap::Bucket *last_list = map->hash_table + map->TAB_SIZE - 1;
        // First iteration of loop below - it might be the end of a non-empty list,
        // other iterations - there might be many empty lists in hash_table.
        while(++it==list->end())
        {
                if(list == last_list) // end of hash_table in map
                    return retval; // end iterator: it=last_list->end()
                it = (++list)->begin();
        }
        return retval;
    }

    ConstIterator& operator--()
    {
        while(it==list->begin())
        {
            if(list == map->hash_table)
               throw std::out_of_range("operator--() on hash map begin iterator.");
            it = (--list)->end();
        }
        --it;
        return *this;
    }

    ConstIterator operator--(int)
    {
        auto retval = *this;
        while(it==list->begin())
        {
            if(list == map->hash_table)
               throw std::out_of_range("operator--(int) on hash map begin iterator.");
            it = (--list)->end();
        }
        --it;
        return retval;
    }

    reference operator*() const
    {
        if(it==list->end())
            throw std::out_of_range("operator*() on hash map end iterator.");
        return *it;
    }

    pointer operator->() const
    {
        return &this->operator*();
    }

    bool operator==(const ConstIterator& other) const
    {
        // no need to check list/map, because std::list iterator has well defined operator==
        return it == other.it;
    }

    bool operator!=(const ConstIterator& other) const
    {
        return !(*this == other);
    }

private:
    typename HashMap::Bucket::iterator it;
    HashMap::Bucket *list;
    const HashMap *map;

    ConstIterator(const typename HashMap::Bucket::iterator &i,
                  HashMap::Bucket *l, const HashMap *m)
        : it(i), list(l), map(m)
    {}
}; // class HashMap<KeyType, ValueType>::ConstIterator

template <typename KeyType, typename ValueType>
class HashMap<KeyType, ValueType>::Iterator : public HashMap<KeyType, ValueType>::ConstIterator
{
public:
    using reference = typename HashMap::reference;
    using pointer = typename HashMap::value_type*;

    explicit Iterator()
    {}

    Iterator(const ConstIterator& other)
        : ConstIterator(other)
    {}

    Iterator& operator++()
    {
        ConstIterator::operator++();
        return *this;
    }

    Iterator operator++(int)
    {
        auto result = *this;
        ConstIterator::operator++();
        return result;
    }

    Iterator& operator--()
    {
        ConstIterator::operator--();
        return *this;
    }

    Iterator operator--(int)
    {
        auto result = *this;
        ConstIterator::operator--();
        return result;
    }

    pointer operator->() const
    {
        return &this->operator*();
    }

    reference operator*() const
    {
        // ugly cast, yet reduces code duplication.
        return const_cast<reference>(ConstIterator::operator*());
    }
}; // class HashMap<KeyType, ValueType>::Iterator

} // namespace aisdi

#endif /* AISDI_MAPS_HASHMAP_H */
