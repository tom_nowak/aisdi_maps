/*
   Tomasz Nowak
   nr albumu: 277117
*/

#include <cstddef>
#include <cstdlib>
#include <iostream>
#include "TreeMap.h"
#include "HashMap.h"
#include <boost/version.hpp> // to check boost version (in old versions there are memory leaks in tests)
#include <map> // to compare aisdi::TreeMap (BST tree) with std::map (red-black tree in GCC implementation)
#include <chrono> // measue time
#include <algorithm> // std::random_shuffle
#include <vector> // to store data used in tests
#include <fstream> // save data from tests to file (then gnuplot will be used to draw plots)
#include <string>

namespace
{
//template <typename K, typename V>
//using Map = aisdi::TreeMap<K, V>;
using Duration = std::chrono::duration<unsigned long long, std::nano>;
using Clock = std::chrono::high_resolution_clock;

// The following parameters are passed as main function arguments.
// These parameters used to be constants, but I changed it.
// They are available in the whole file (I find passing them around needless).
// Warning - tests are computation-heavy, so do not set too big values.
unsigned NUMBER_OF_TESTS; // to average results of random key shuffling
unsigned NUMBER_OF_ELEMENTS_IN_MAP; // to see dependence of time on number of elements
unsigned NUMBER_OF_MAPS; // to minimise result of single time measurement error
// These parameters depend on the 3 above:
unsigned NUMBER_OF_KEYS;
unsigned NUMBER_OF_TIMES_SUMS; // needed to normalize array times

// Vectors (changed from arrays after letting the user set parameters)
// available in the whole file (I find passing them around needless):
std::vector<unsigned> keys;
std::vector<unsigned long long> insertionTimes;
std::vector<unsigned long long> iterationTimes;
std::vector<unsigned long long> removalTimes;

int myAtoi(char *c)
{
    int tmp = std::atoi(c);
    if(tmp<1)
    {
        std::cerr << "Only positive arguments are accepted.\n";
        exit(1);
    }
    return tmp;
}

void initializeParamerers(char **argv)
{
    NUMBER_OF_TESTS = myAtoi(argv[1]);
    NUMBER_OF_ELEMENTS_IN_MAP = myAtoi(argv[2]);
    NUMBER_OF_MAPS = myAtoi(argv[3]);
    NUMBER_OF_KEYS = NUMBER_OF_MAPS*NUMBER_OF_ELEMENTS_IN_MAP;
    NUMBER_OF_TIMES_SUMS = NUMBER_OF_MAPS*NUMBER_OF_TESTS;
}

void initializeVectors()
{
    keys.resize(NUMBER_OF_KEYS);
    insertionTimes.resize(NUMBER_OF_ELEMENTS_IN_MAP);
    iterationTimes.resize(NUMBER_OF_ELEMENTS_IN_MAP);
    removalTimes.resize(NUMBER_OF_ELEMENTS_IN_MAP);
    for(unsigned i=0; i<NUMBER_OF_KEYS; ++i)
        keys[i] = i;
}

// Inherit after std::map to be able to call tests in the same way for as for aisdi::TreeMap
struct StdMap : public std::map<unsigned, long long int>
{
    void remove(unsigned key) // alias for standard function erase (because aisdi maps have function remove)
    {
        std::map<unsigned, long long int>::erase(key);
    }
};

template <typename Map> // StdMap, aisdi::TreeMap<unsigned,long long int>, aisdi::HashMap<unsigned,long long int>
void performTest()
{
    std::vector<Map> maps(NUMBER_OF_MAPS);
    Clock::time_point start, stop;
    Duration dur;
    std::random_shuffle(keys.begin(), keys.end()); // new test - shuffle everything
    unsigned keyIndex = 0, n = 0;
    for(; n<NUMBER_OF_ELEMENTS_IN_MAP; ++n)
    {
        // insert with random key:
        start = Clock::now();
        for(unsigned m=0; m<NUMBER_OF_MAPS; ++m)
        {
            unsigned key = keys[keyIndex++];
            (maps[m]) [key] = -static_cast<long long int>(key);
        }
        stop = Clock::now();
        dur = std::chrono::duration_cast<Duration>(stop-start);
        insertionTimes[n] += dur.count();

        // iterate for each map element:
        start = Clock::now();
        for(unsigned m=0; m<NUMBER_OF_MAPS; ++m)
        {
            for(auto &it : maps[m])
                ++it.second;
        }
        stop = Clock::now();
        dur = std::chrono::duration_cast<Duration>(stop-start);
        iterationTimes[n] += dur.count();
    }
    keyIndex = 0;
    n = NUMBER_OF_ELEMENTS_IN_MAP - 1;
    do
    {
        // find element with random key and remove it:
        start = Clock::now();
        for(unsigned m=0; m<NUMBER_OF_MAPS; ++m)
            maps[m].remove(keys[keyIndex++]);
        stop = Clock::now();
        Duration dur = std::chrono::duration_cast<Duration>(stop-start);
        removalTimes[n] += dur.count();
    } while(n-- != 0);
}

void saveTestOutput(const std::string &filename, std::vector<unsigned long long> &vec)
{
    std::ofstream file(filename);
    if(!file.is_open())
    {
        std::cerr << "could not open file: " << filename << "\n";
        exit(1);
    }
    for(unsigned i=0; i<NUMBER_OF_ELEMENTS_IN_MAP; ++i)
        file << vec[i]/NUMBER_OF_TIMES_SUMS << "\n";
    file.close();
}

void saveAllTestsOutput(const std::string &filenamePrefix)
{
    std::string filename = filenamePrefix + "_random_insert_" +
                           std::to_string(NUMBER_OF_TESTS) + "_" +
                           std::to_string(NUMBER_OF_ELEMENTS_IN_MAP) + "_" +
                           std::to_string(NUMBER_OF_MAPS) + ".txt";
    saveTestOutput(filename, insertionTimes);
    filename = filenamePrefix + "_iterate_for_each_" +
                           std::to_string(NUMBER_OF_TESTS) + "_" +
                           std::to_string(NUMBER_OF_ELEMENTS_IN_MAP) + "_" +
                           std::to_string(NUMBER_OF_MAPS) + ".txt";
    saveTestOutput(filename, iterationTimes);
    filename = filenamePrefix + "_random_find_and_remove_" +
                           std::to_string(NUMBER_OF_TESTS) + "_" +
                           std::to_string(NUMBER_OF_ELEMENTS_IN_MAP) + "_" +
                           std::to_string(NUMBER_OF_MAPS) + ".txt";
    saveTestOutput(filename, removalTimes);
}

#ifdef AISDI_HOMEWORK
void homeworkPrintMapBreadth()
{
    aisdi::TreeMap<int, const char*> map;
    std::cout << "AISDI homework - print map breadth-first\n";
    map[10] = "(first)";
    map[5] = "(left under first)";
    map[7] = "(right under left under first)";
    map[15] = "(right under first)";
    map[16] = "(right under right under first)";
    map[3] = "(left under left under first)";
    map[20] = "(the most right)";
    struct Functor
    {
        void operator() (const aisdi::TreeMap<int, const char*>::value_type &v)
        {
            std::cout << v.first << " " << v.second << "\n";
        }
    } functorPrint;
    map.forEachBreadthFirst(functorPrint);
}
#endif
} // local namespace

int main(int argc, char **argv)
{
    if(argc!=4)
    {
        std::cout << "Usage: " << argv[0] << " <3 arguments>:\n"
                     " <number of tests (to reduce randomness)>\n"
                     " <max number of elements in map>\n"
                     " <number of maps in each test (to reduce mesurement error)>\n"
                     "Warning - tests are computation-heavy, so do not set too big values.\n";
        return 1;
    }
    std::cout << "Initializing tests...\n";
    std::srand(unsigned(std::time(0)));
    initializeParamerers(argv);
    initializeVectors();
    try
    {
        std::cout << "Testing performance of std::map (to compare it with aisdi maps)...\n";
        for(unsigned i=0; i<NUMBER_OF_TESTS; ++i)
            performTest<StdMap>();
        saveAllTestsOutput("std::map");
    }
    catch(const std::exception &ex) { std::cerr << "Exception in std::map : " << ex.what() << "\n"; }
    try
    {
        std::cout << "Testing performance of aisdi::TreeMap...\n";
        for(unsigned i=0; i<NUMBER_OF_TESTS; ++i)
            performTest<aisdi::TreeMap<unsigned, long long int>>();
        saveAllTestsOutput("aisdi::TreeMap");
    }
    catch(const std::exception &ex) { std::cerr << "Exception in TreeMap : " << ex.what() << "\n"; }
    try
    {
        std::cout << "Testing performance of aisdi::HashMap...\n";
        for(unsigned i=0; i<NUMBER_OF_TESTS; ++i)
            performTest<aisdi::HashMap<unsigned, long long int>>();
        saveAllTestsOutput("aisdi::HashMap");
    }
    catch(const std::exception &ex) { std::cerr << "Exception in HashMap : " << ex.what() << "\n"; }
    std::cout << "Results of performance tests saved.\n"
                 "There is number of nanoseconds in each line, for increasing number of elements.\n"
                 "You can use gnuplot to draw plots\n\n";
#ifdef AISDI_HOMEWORK
    homeworkPrintMapBreadth();
#endif
    /*
    std::cout << "Using Boost "
              << BOOST_VERSION / 100000     << "."  // major version
              << BOOST_VERSION / 100 % 1000 << "."  // minor version
              << BOOST_VERSION % 100                // patch level
              << std::endl;
    */
    return 0;
}
